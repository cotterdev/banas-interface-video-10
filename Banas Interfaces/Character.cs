﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace Banas_Interfaces
{
    class Character : ImyFirstInterface
    {
        public int Health { get; set; }
        public double Damage { get; set; }

        #region ctor
        public Character(int health = 0, double damage = 20)
        {
            Health = health;
            Damage = damage;
        }

        #endregion

        public void Attach()
        {
            Log.Information("You are attacking!");
        }

        public void Move()
        {
            Log.Information("You moved!!!");
        }


    }
}
