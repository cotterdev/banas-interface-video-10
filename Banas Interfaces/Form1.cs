﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog;

namespace Banas_Interfaces
{
    public partial class Form1 : Form
    {
        

        public Form1()
        {
            InitializeComponent();

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()                                                  // Needed to install serilog.console from NuGet
                                                                                    //.WriteTo.File("tobyLog.txt", rollingInterval: RollingInterval.Day)  // This is the "sink".  Req'd when creating the logger object. Serilog.File installed from NuGet
                .CreateLogger();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Log.Information("Works!");

            Character wizard = new Character();
            Log.Information($"Wizard health: {wizard.Health.ToString()}");
            Log.Information($"Wizard damage: {wizard.Damage.ToString()}");
            wizard.Health += 21;
            wizard.Damage += 11;
            Log.Information($"Wizard health: {wizard.Health.ToString()}");
            Log.Information($"Wizard damage: {wizard.Damage.ToString()}");

            Character axeMan = new Character(23, 43);
            Log.Information($"Axeman health: {axeMan.Health.ToString()}");
            Log.Information($"Axeman damage: {axeMan.Damage.ToString()}");
            axeMan.Health += 21;
            axeMan.Damage += 11;
            Log.Information($"Axeman health: {axeMan.Health.ToString()}");
            Log.Information($"Axeman damage: {axeMan.Damage.ToString()}");
        }
    }
}
