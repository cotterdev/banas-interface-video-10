﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banas_Interfaces
{
    interface ImyFirstInterface
    {
        int Health { get; set; }
        double Damage { get; set; }
        void Move();
        void Attach();
    }
}
